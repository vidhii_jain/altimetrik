function update_vote(article_id) {
    $.ajax({
        headers: {'Content-Type': 'application/json'},
        url: '/article/set/upvote_article/',
        data: JSON.stringify({'article_id': article_id}),
        error: function () {
            alert("Invalid Request");
        },
        success: function (data) {
            alert("Successfully Updated Vote Count!");
            window.location.reload();
        },
        type: 'POST'
    });
}


function populate_article_list(results) {
    for (i = 0; i < results.length; i++) {
        html = '<div class="col-sm-12"><div class="col-sm-8">' + results[i].title + '</div><div class="col-sm-2">' + results[i].no_of_votes +
            '</div><div class="col-sm-2"><button type="button" class="btn btn-primary " onclick="update_vote(' + results[i].id +
            ');"> Up Vote' + '</button></div><hr/></div>';
        $(".article").append(html);
    }

}

function post_article() {
    var title = $('#form_article_title').val();
    var author = $('#form_article_user').val();
    var content = $('#form_article_content').val();
    var data = JSON.stringify({'title': title, 'author': author, 'content': content});
    $.ajax({
        type: "POST",
        url: '/article/set/',
        data: data,
        contentType: "application/json",
        success: function (data) {
            alert("Successfully Created Article");

        },
        error: function () {
            alert("Error Creating Article");
        }
    });
}