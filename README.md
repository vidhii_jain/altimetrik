#Altimetril Article Assignment 

#installation guide 
pip install -r requirements.txt
./manage.py makemigrations
./manage.py migrate
./manage.py createsuperuser - Enter username and password
./manage.py runserver 

#User is created using the django admin

#django admin can be accessed using
http://localhost:8000/admin/


2 Screens have been created: 

#To create article
http://localhost:8000/article/list/
the above URL calls the template - create.html

#To list article
http://localhost:8000/article/create/
the above URL calls the template - list.html

#Postman Link for the API Endpoints - 
https://www.getpostman.com/collections/e086f9df6b9a885a3acf

Has the following APIS :

#GET request - List article API 
http:/localhost:8000/article/set/

#POST request - Create Article API
http://localhost:8000/article/set/

#POST request - UpVote Article
http://localhost:8000/article/set/upvote_article/
