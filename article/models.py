from django.db import models

class Article(models.Model):
    """
    Article model
    """
    title = models.CharField(max_length=200, null=False, blank=False)
    author = models.CharField(max_length=200, null=False, blank=False)
    content = models.TextField(null=True, blank=True)
    no_of_votes = models.BigIntegerField(default=0, blank=True)

    class Meta:
        verbose_name = "Article"
        verbose_name_plural = "Articles"
        app_label = 'article'
        ordering = ('-no_of_votes', )

    def __str__(self):
        return self.title
