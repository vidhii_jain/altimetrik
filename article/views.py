from __future__ import unicode_literals
from django.http import HttpResponse
from .serializers import ArticleSerializer
from rest_framework.response import Response
from .models import Article
from annoying.functions import get_object_or_None
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.viewsets import GenericViewSet
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
import json
from django.views.generic import TemplateView


class ArticleListView(TemplateView):
    template_name = "list.html"


class ArticleCreateView(TemplateView):
    template_name = "create.html"


@method_decorator(csrf_exempt, name='dispatch')
class ArticleViewSet(GenericViewSet):
    """
    pk: Article id
    """
    queryset = Article.objects.all()

    def list(self, request, *args, **kwargs):
        serializer = ArticleSerializer(self.queryset.all(), many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = ArticleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"message": "Successfully Created"}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['post'], detail=False)
    def upvote_article(self,request):
        data = json.loads(request.body)
        id = data.get('article_id', None)
        article = get_object_or_None(Article, pk=int(id))
        if article:
            article.no_of_votes += 1
            article.save()
            return HttpResponse({"message": "Successfully Updated"}, status=status.HTTP_200_OK)
        return HttpResponse({"message": "Not Found"}, status=status.HTTP_400_BAD_REQUEST)





