from rest_framework.routers import DefaultRouter
from .views import ArticleViewSet, ArticleListView, ArticleCreateView
from django.conf.urls import url

urlpatterns = [
    url(r'list/$', ArticleListView.as_view(), name='article-list'),
    url(r'create/$', ArticleCreateView.as_view(), name='article-create'),
]


router = DefaultRouter()
router.register(r'set', ArticleViewSet)

urlpatterns += router.urls