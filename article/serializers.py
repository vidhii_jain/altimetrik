from .models import *
from rest_framework import serializers

class ArticleSerializer(serializers.ModelSerializer):
    """
    Article Serializer
    """
    class Meta:
        model = Article
        exclude = ()
